# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.24.2
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
# ppc64le: FTBFS: build constraints exclude all Go files in [...]
# riscv64: modernc.org/libc@v1.14.1 build constraints exclude all Go files
arch="$arch !s390x !ppc64le !riscv64"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"

build() {
	make build VERSION=$pkgver
}

check() {
	make test
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
9140fa0bf56c4add324072063cd9a6961288e8cf880f36761953fc33f63165924b5ef2381e7ae2e2be9402e3654fddd14ccf6979e8808090c65c684c2d9243a3  trivy-0.24.2.tar.gz
"
